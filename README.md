# IG-04

A retro-styled FPS based in the Star Wars universe, where you control the bounty-hunting droid IG-04, also known as Sar Kalist, in his attempt to fulfill a mission delivered to him by the Separatist Alliance : kill Senator Koth Mendis.